// start Soal No. 1
console.log('== Soal No. 1 ==');
// Code di sini
function range (num1, num2) {
    var numbers = [];
    if(typeof num1!=='undefined' && typeof num2!=='undefined') {
        if(num1>num2) {
            for(var i=num1; i>=num2; i--) {
                numbers.push(i);
            }
        } else {
            for(var i=num1; i<=num2; i++) {
                numbers.push(i);
            }
        }
    } else {
        numbers.push(-1);
    }

    return numbers;
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// end Soal No. 1

// start Soal No. 2
console.log('\n== Soal No. 2 ==');
// Code di sini
function rangeWithStep (num1, num2, step) {
    var numbers = [];
    if(num1>num2) {
        for(var i=num1; i>=num2; i-=step) {
            numbers.push(i);
        }
    } else {
        for(var i=num1; i<=num2; i+=step) {
            numbers.push(i);
        }
    }

    return numbers;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
// end Soal No. 2

// start Soal No. 3
console.log('\n== Soal No. 3 ==');
// Code di sini
function sum (num1, num2, step) {
    var numbers = [];

    if(typeof num1!=='undefined' && typeof num2!=='undefined' && typeof step!=='undefined') {
        numbers = rangeWithStep(num1, num2, step);
    } else if(typeof num1!=='undefined' && typeof num2!=='undefined') {
        numbers = range(num1, num2);
    } else if(typeof num1!=='undefined') {
        numbers.push(num1);
    } else {
        numbers.push(0);
    }

    var result = 0;
    for(var i=0; i<numbers.length; i++){
        result+=numbers[i];
    }

    return result;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// end Soal No. 3

// start Soal No. 4
console.log('\n== Soal No. 4 ==');

function dataHandling(input) {
    for (var i=0; i < input.length; i++){
        console.log("Nomor ID: "+input[i][0]);
        console.log("Nama Lengkap: "+input[i][1]);
        console.log("TTL: "+input[i][2]+" "+input[i][3]);
        console.log("Hobi: "+input[i][4]);
        console.log("");
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

dataHandling(input);
// end Soal No. 4

// start Soal No. 5
console.log('\n== Soal No. 5 ==');
// Code di sini
function balikKata(teks) {
    teks_terbalik = '';
    for(var i=teks.length-1; i>=0; i--) {
        teks_terbalik += teks[i];
    }

    return teks_terbalik;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
// end Soal No. 5

// start Soal No. 6
console.log('\n== Soal No. 6 ==');

// konversi bulan angka ke string
function konversiBulan(bulan) {
    var bulanString = '';
    switch (bulan) {
        case '01': { 
            bulanString = 'Januari';
            break;
        }
        case '02': { 
            bulanString = 'Februari';
            break;
        }
        case '03': { 
            bulanString = 'Maret';
            break;
        }
        case '04': { 
            bulanString = 'April';
            break;
        }
        case '05': { 
            bulanString = 'Mei';
            break;
        }
        case '06': { 
            bulanString = 'Juni';
            break;
        }
        case '07': { 
            bulanString = 'Juli';
            break;
        }
        case '08': { 
            bulanString = 'Agustus';
            break;
        }
        case '09': { 
            bulanString = 'September';
            break;
        }
        case '10': { 
            bulanString = 'Oktober';
            break;
        }
        case '11': { 
            bulanString = 'November';
            break;
        }
        case '12': { 
            bulanString = 'Desember';
            break;
        }
    }
    return bulanString;
}

//fungsi utama
function dataHandling2(input) {
    //case1
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input);
    //Output: ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]

    //case2
    var bulan = '';
    bulan = input[3].split("/");
    console.log(konversiBulan(bulan[1]));
    //Output: Mei

    //case3
    // Mengurutkan secara Descending
    var bulan_desc = input[3].split("/");
    bulan_desc.sort(function (value1, value2) { return value2-value1 } );
    console.log(bulan_desc);
    //Output: ["1989", "21", "05"]

    //case4
    console.log(bulan.join("-"));
    //Output: 21-05-1989

    //case5
    console.log(input[1].slice(0,15));
    //Output: Roman Alamsyah
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(input);
// end Soal No. 6