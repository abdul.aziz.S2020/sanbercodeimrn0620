import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillItem extends React.Component {
  render() {
    let skill = this.props.skill;
    return(
      <TouchableOpacity>
        <View style={styles.skillItem}>
          <View style={styles.colomItem}><Icon name={skill.iconName} size={60} style={styles.icon} /></View>
          <View style={styles.colomItem} width={140}>
            <Text style={styles.skillTitle}>{skill.skillName}</Text>
            <Text style={styles.skillSubtitle}>{skill.categoryName}</Text>
            <Text style={styles.skillPersen}>{skill.percentageProgress}</Text>
          </View>
          <View style={styles.colomItem}><Icon name="chevron-right" size={70} style={styles.icon} /></View>
        </View>
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  icon: {
    color: '#003366',
    marginHorizontal: 5
  },
  colomItem: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  skillItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
    marginBottom: 5,
    paddingHorizontal: 8,
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
  },
  skillTitle: {
    fontSize: 20, fontWeight: 'bold', color: '#003366'
  },
  skillSubtitle: {
    fontSize: 14, fontWeight: 'bold', color: '#3EC6FF'
  },
  skillPersen: {
    fontSize: 40, fontWeight: 'bold', color: '#FFFFFF', textAlign: 'right'
  }
});

