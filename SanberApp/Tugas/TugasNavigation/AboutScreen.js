import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default ({navigation}) => (
  <View style={styles.container}>
    <ScrollView>
      <View style={styles.navBar}>
        <Text style={styles.title}>Tentang Saya</Text>
      </View>
      <View style={styles.body}>
        <View style={styles.profil}>
          <Image source={require('./images/foto.jpg')} style={styles.imgProfil} />
          <Text style={{fontSize: 22, fontWeight: 'bold', color: '#003366'}}>Abdul Aziz</Text>
          <Text style={{fontSize: 16, fontWeight: 'bold', color: '#3EC6FF'}}>React Native Developer</Text>
        </View>
        <View style={styles.listItem}>
          <Text style={styles.listItemTitle}>Portfolio</Text>
          <View style={styles.lineStyle} />
          <View style={styles.portfolio}>
            <TouchableOpacity style={styles.portfolioItem}>
              <Icon name="gitlab" size={40} style={styles.icon} />
              <Text style={styles.portfolioTitle}>@abdul.aziz.S2020</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.portfolioItem}>
              <Icon name="github" size={40} style={styles.icon} />
              <Text style={styles.portfolioTitle}>@zizsprikitiw</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#003366',
    marginBottom: 10
  },
  navBar: {
    backgroundColor: 'white',
    paddingTop: 80,
    alignItems: 'center'
  },
  body: {
    flex: 1,
    alignItems: 'center',
  },
  profil: {
    marginBottom: 20,
    alignItems: 'center',
  },
  imgProfil: {
    width:150,
    height: 150,
    borderRadius: 180,
    marginBottom: 20
  },
  listItem: {
    flex: 0,
    backgroundColor: '#EFEFEF',
    width: 320,
    marginBottom: 20
  },
  listItemTitle: {
    fontSize: 18,
    color: '#003366',
    paddingLeft: 10,
    paddingTop: 5
  },
  icon: {
    color: '#3EC6FF'
  },
  lineStyle:{
    borderWidth: 0.5,
    borderColor:'#003366',
    marginTop: 5,
    marginBottom:15,
    marginHorizontal: 10
  },
  portfolio: {
    height: 60,
    marginBottom: 20,
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  portfolioItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  portfolioTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#003366',
    paddingTop: 4,
  }
});