import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import LoginScreen from "./LoginScreen";
import SkillScreen from "./SkillScreen";
import AboutScreen from "./AboutScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const SkillStack = createStackNavigator();
const AboutStack = createStackNavigator();
const ProjectStack = createStackNavigator();
const AddStack = createStackNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="LoginScreen" component={LoginScreen} options={{title: "Login Screen"}} />
  </LoginStack.Navigator>
);

const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="SkillScreen" component={SkillScreen} options={{title: "Skill Screen"}} />
  </SkillStack.Navigator>
);

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="AboutScreen" component={AboutScreen} options={{title: "About Screen"}} />
  </AboutStack.Navigator>
);

const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="ProjectScreen" component={ProjectScreen} options={{title: "Project Screen"}} />
  </ProjectStack.Navigator>
);

const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="AddScreen" component={AddScreen} options={{title: "Add Screen"}} />
  </AddStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="SkillScreen" component={SkillStackScreen} />
    <Tabs.Screen name="ProjectScreen" component={ProjectStackScreen} />
    <Tabs.Screen name="AddScreen" component={AddStackScreen} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

const DrawScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="SkillScreen" component={TabsScreen} />
    <Drawer.Screen name="AboutScreen" component={AboutStackScreen} />
  </Drawer.Navigator>
);

export default () => (

  <NavigationContainer>
    {/*<Drawer.Navigator>
      <Drawer.Screen name="LoginScreen" component={TabsScreen} />
    </Drawer.Navigator>*/}
    <AuthStack.Navigator>
      <AuthStack.Screen name="LoginScreen" component={LoginStackScreen} options={{title: "Login Screen"}} />
      <AuthStack.Screen name="SkillScreen" component={DrawScreen} options={{title: "Skill Screen"}} />
    </AuthStack.Navigator>
  </NavigationContainer>
);
