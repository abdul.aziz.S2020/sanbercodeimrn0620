import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  FlatList
} from 'react-native';
import SkillItem from './components/SkillItem';
import data from './skillData.json';

export default ({navigation}) => (
  <View style={styles.container}>
    <ScrollView>
      <View style={styles.navBar}>
        <Image source={require('./images/logo.png')} style={{width: 150, height: 50}} />
      </View>
      <View style={styles.body}>
        <View style={styles.profil}>
          <View style={styles.profilItem}>
            <Image source={require('./images/foto.jpg')} style={styles.imgProfil} />
          </View>
          <View style={styles.profilItem}>
            <Text>Hai,</Text>
            <Text style={styles.nameText}>Abdul Aziz,</Text>
          </View>
        </View>

        <Text style={styles.title}>SKILL</Text>
        <View style={styles.lineStyle} />

        <View style={styles.category}>
          <View style={styles.categoryItem}>
            <Text style={styles.categoryText}>Library/ Framework</Text>
          </View>
          <View style={styles.categoryItem}>
            <Text style={styles.categoryText}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.categoryItem}>
            <Text style={styles.categoryText}>Teknologi</Text>
          </View>
        </View>

        <View style={styles.skill}>
          <FlatList
            data={data.items}
            keyExtractor={(item)=>item.id}
            renderItem={(skill)=><SkillItem skill={skill.item} />} 
          />
        </View>
      </View>
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  navBar: {
    backgroundColor: 'white',
    padding: 15,
    alignSelf: 'flex-end'
  },
  body: {
    paddingHorizontal: 15
  },
  title: {
    fontSize: 30,
    color: '#003366'
  },
  nameText: {
    fontSize: 16,
    color: '#003366'
  },
  profil: {
    flexDirection: 'row',
    marginBottom: 15
  },
  profilItem: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  imgProfil: {
    width:30,
    height: 30,
    borderRadius: 50,
    marginRight: 10
  },
    lineStyle:{
    borderWidth: 2,
    borderColor:'#3EC6FF',
    marginVertical: 5
  },
  category: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  categoryItem: {
    padding: 4,
    paddingHorizontal: 8,
    backgroundColor: '#B4E9FF',
    borderRadius: 8
  },
  categoryText: {
    color: '#003366',
    fontSize: 12
  },
  skill: {
    flexDirection: 'column'
  }
});