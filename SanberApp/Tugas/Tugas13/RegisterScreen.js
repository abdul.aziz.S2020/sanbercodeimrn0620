import React, { Component } from 'react';
import { 
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TextInput,
  TouchableOpacity
} from 'react-native';

export default class RegisterScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image source={require('./images/logo.png')} style={{width: 300, height: 100}} />
        </View>
        <View style={styles.body}>
            <Text style={styles.title}>Register</Text>
            <View style={styles.formGroup}>
                <Text style={styles.label}>Username</Text>
                <TextInput
                    style={{ width:300, height: 40, borderColor: '#003366', borderWidth: 1 }}
                />
            </View>
            <View style={styles.formGroup}>
                <Text style={styles.label}>Email</Text>
                <TextInput
                    style={{ width:300, height: 40, borderColor: '#003366', borderWidth: 1 }}
                />
            </View>
            <View style={styles.formGroup}>
                <Text style={styles.label}>Password</Text>
                <TextInput
                    style={{ width:300, height: 40, borderColor: '#003366', borderWidth: 1 }}
                />
            </View>
            <View style={styles.formGroup}>
                <Text style={styles.label}>Ulangi Password</Text>
                <TextInput
                    style={{ width:300, height: 40, borderColor: '#003366', borderWidth: 1 }}
                />
            </View>
            <TouchableOpacity onPress={() => {this.sendRegisterData()}} style={styles.regButton}>
                <Text style={{textAlign:'center', fontSize:16, color:'white'}}>Daftar</Text>
            </TouchableOpacity>
            <Text style={{color: '#3EC6FF', fontSize:18}}>atau</Text>
            <TouchableOpacity onPress={() => {this.sendLoginData()}} style={styles.logButton}>
                <Text style={{textAlign:'center', fontSize:16, color:'white'}}>Masuk ?</Text>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 50,
    alignItems: 'center'
  },
  body: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    color: '#003366',
    marginBottom: 30,
  },
  formGroup: {
    marginBottom: 5,
  },
  label: {
    color: '#003366',
  },
  logButton: {
    borderRadius: 10,
    backgroundColor: '#3EC6FF',
    paddingHorizontal: 25,
    paddingVertical: 4,
    marginVertical: 10
  },
  regButton: {
    borderRadius: 10,
    backgroundColor: '#003366',
    paddingHorizontal: 25,
    paddingVertical: 4,
    marginVertical: 10
  },
});