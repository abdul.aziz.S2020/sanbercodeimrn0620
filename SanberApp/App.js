import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import YoutubeUI from './Tugas/Quiz3/index';

export default function App() {
  return (
    <YoutubeUI />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});