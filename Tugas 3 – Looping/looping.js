// start Soal No. 1
console.log('== Soal No. 1 ==');
console.log('LOOPING PERTAMA');
var i = 0;
var angka = 2;

while(i < 10) {
  console.log(angka+' - I love coding');
  angka = angka + 2;
  i++;
}

console.log('LOOPING KEDUA');
while(i > 0) {
    angka = angka - 2;
    console.log(angka+' - I will become a mobile developer');
    i--;
}
// end Soal No. 1

// start Soal No. 2
console.log('\n== Soal No. 2 ==');
var teks = '';
for(var angka = 1; angka <= 20; angka++) {
    if(angka%2==1 && angka%3!=0) {
        teks = 'Santai';
    } else if (angka%2==0) {
        teks = 'Berkualitas';
    } else if (angka%2==1 && angka%3==0) {
        teks = 'I Love Coding';
    }
    console.log(angka+' - '+teks);
} 
// end Soal No. 2

// start Soal No. 3
console.log('\n== Soal No. 3 ==');
var teks = '';
for(var i = 1; i <= 4; i++) {
    for(var j = 1; j <= 8; j++) {
        teks += '#';
    }
    console.log(teks);
    teks = '';
} 
// end Soal No. 3

// start Soal No. 4
console.log('\n== Soal No. 4 ==');
var teks = '';
for(var i = 1; i <= 7; i++) {
    for(var j = 1; j <= i; j++) {
        teks += '#';
    }
    console.log(teks);
    teks = '';
} 
// end Soal No. 4

// start Soal No. 5
console.log('\n== Soal No. 5 ==');
var teks = '';
for(var i = 1; i <= 8; i++) {
    for(var j = 1; j <= 4; j++) {
        if(i%2==1) {
            teks += ' #';
        } else {
            teks += '# ';
        }
    }
    console.log(teks);
    teks = '';
} 
// end Soal No. 5