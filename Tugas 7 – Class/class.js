// start Soal No. 1
console.log('== Soal No. 1 ==');
class Animal {
    // Code class di sini
    constructor(param) {
        this.name = param;
    }

    get legs() {
        return 4;
    }

    get cold_blooded() {
        return false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
// end Soal No. 1

// start Soal No. 2
console.log('\n== Soal No. 2 ==');
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(param) {
        super(param);
        this._param = param;
    }

    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(param) {
        super(param);
        this._param = param;
    }

    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
// end Soal No. 2

// start Soal No. 3
console.log('\n== Soal No. 3 ==');
class Clock {
    constructor({ template }) {
        this.template = template;
    }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 