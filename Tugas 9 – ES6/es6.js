// start Soal No. 1
console.log('== Soal No. 1 ==');

// ES5 code
/*const golden = function goldenFunction(){
    console.log("this is golden!!")
}*/

// ES6 code
const golden = () => {
    return console.log("this is golden!!");
}

golden();
// end Soal No. 1

// start Soal No. 2
console.log('\n== Soal No. 2 ==');

// ES5 code
/*const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
}*/

// ES6 code
const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName: () => {
        console.log(firstName + " " + lastName);
      }
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName() 
// end Soal No. 2

// start Soal No. 3
console.log('\n== Soal No. 3 ==');

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// ES5 code
/*const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;*/

// ES6 code
const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation);
// end Soal No. 3

// start Soal No. 4
console.log('\n== Soal No. 4 ==');

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// ES5 code
//const combined = west.concat(east)

// ES6 code
let combined = [...west, ...east]

//Driver Code
console.log(combined)
// end Soal No. 4

// start Soal No. 5
console.log('\n== Soal No. 5 ==');

const planet = "earth"
const view = "glass"

// ES5 code
/*var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'*/

// ES6 code
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 
// end Soal No. 5