// start Soal No. 1
console.log('== Soal No. 1 ==');
function arrayToObject(arr) {
    // Code di sini
    var now = new Date();
    var thisYear = now.getFullYear(); // 2020 (tahun sekarang) 
    for(var i=0; i<arr.length; i++) {
        var umur;
        if(thisYear < arr[i][3] || arr[i][3] === undefined) {
            umur = "Invalid birth year";
        } else {
            umur = thisYear-arr[i][3];
        }

        var peopleObj = {
            firstName : arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: umur
        }
        console.log(`${i+1}. ${peopleObj.firstName} ${peopleObj.lastName}: `,peopleObj);
    }
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
// end Soal No. 1

// start Soal No. 2
console.log('\n== Soal No. 2 ==');
function shoppingTime(memberId, money) {
    // you can only write your code here!
    var barang = [
        {nama : "Sepatu Stacattu", harga : 1500000},
        {nama : "Baju Zoro", harga : 500000},
        {nama : "Baju H&N", harga : 250000},
        {nama : "Sweater Uniklooh", harga : 175000},
        {nama : "Casing Handphone", harga : 50000},
    ];
    
    var uang = money;
    var barangObj = [];
    for(var i=0; i<barang.length; i++) {
        if(uang>=barang[i].harga) {
            barangObj.push(barang[i].nama);
            uang -= barang[i].harga;
        }
    }

    var hasil = "";
    if(memberId === undefined || memberId === '') {
        hasil = "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if(money<50000) {
        hasil = "Mohon maaf, uang tidak cukup";
    } else {
        var peopleObj = {
            memberId : memberId,
            money: money,
            listPurchased: barangObj,
            changeMoney: uang
        }

        hasil = peopleObj;
    }

    return hasil;
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
// end Soal No. 2

// start Soal No. 3
console.log('\n== Soal No. 3 ==');
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var penumpangObj = [];
    for(var i=0; i<arrPenumpang.length; i++) {
        var ongkos = 0;
        for(var j=0; j<rute.length; j++) {
            if(rute[j]==arrPenumpang[i][1]) {
                ongkos = 0;
            } else if(rute[j]==arrPenumpang[i][2]) {
                ongkos += 2000;
                break;
            } else {
                ongkos += 2000;
            }
        }
        penumpangObj.push({penumpang: arrPenumpang[i][0], naikDari: arrPenumpang[i][1], tujuan: arrPenumpang[i][2], bayar: ongkos}); 
    }

    return penumpangObj;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
  // end Soal No. 3