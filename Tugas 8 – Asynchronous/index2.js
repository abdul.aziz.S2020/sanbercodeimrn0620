var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var waktu = 10000;
function fungsiBaca(x) {
    if(x == books.length) {
        return 0;
    }

    readBooksPromise(waktu, books[x])
    .then(function (fulfilled){
        waktu -= books[x].timeSpent
        fungsiBaca(x+1)
    })
}

fungsiBaca(0);