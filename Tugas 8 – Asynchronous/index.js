// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var waktu = 10000;
function fungsiBaca(x) {
    if(x == books.length) {
        return 0;
    }

    readBooks(waktu, books[x], function(callback){
        waktu -= books[x].timeSpent
        fungsiBaca(x+1);
    });
}

fungsiBaca(0);