// start Soal No. 1
var nama = "";
var peran = "";
console.log('== Soal No. 1 ==');

if(nama=='') {
    // Output untuk Input nama = '' dan peran = ''
    console.log("Nama harus diisi!");
} else if(nama!='' && peran=='') {
    //Output untuk Input nama = 'John' dan peran = ''
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
} else {
    if (peran=='Penyihir') {
        //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran=='Guard') {
        //Output untuk Input nama = 'Jenita' dan peran 'Guard'
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peran=='Werewolf') {
        //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
        console.log("Selamat datang di Dunia Werewolf, "+nama);
        console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!");
    }
}
// end Soal No. 1

// start Soal No. 2
var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var bulanString = '';
console.log('\n== Soal No. 2 ==');

// konversi bulan angka ke string
switch (bulan) {
    case 1: { 
        bulanString = 'Januari';
        break;
    }
    case 2: { 
        bulanString = 'Februari';
        break;
    }
    case 3: { 
        bulanString = 'Maret';
        break;
    }
    case 4: { 
        bulanString = 'April';
        break;
    }
    case 5: { 
        bulanString = 'Mei';
        break;
    }
    case 6: { 
        bulanString = 'Juni';
        break;
    }
    case 7: { 
        bulanString = 'Juli';
        break;
    }
    case 8: { 
        bulanString = 'Agustus';
        break;
    }
    case 9: { 
        bulanString = 'September';
        break;
    }
    case 10: { 
        bulanString = 'Oktober';
        break;
    }
    case 11: { 
        bulanString = 'November';
        break;
    }
    case 12: { 
        bulanString = 'Desember';
        break;
    }
}

if(hari < 1 || hari > 31) {
    // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
    console.log("Masukkan tanggal dengan angka antara 1-31!");
} else if(bulan < 1 || bulan > 12) {
    // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
    console.log("Masukkan bulan dengan angka antara 1-12!");
} else if(tahun < 1900 || tahun > 2200) {
    // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
    console.log("Masukkan tahun dengan angka antara 1900-2200!");
} else {
    // hasil yang ditampilkan dari inputan yang sesuai
    console.log(hari+" "+bulanString+" "+tahun);
}
// end Soal No. 2